/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:00 by triviere          #+#    #+#             */
/*   Updated: 2016/01/29 13:35:58 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __MALLOC_H__
# define __MALLOC_H__

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/mman.h>
# include <pthread.h>

# define TINY 4096
# define SMALL 16777216

# define NB_PAGE 100

typedef struct s_mheader	t_mheader;
typedef struct s_mmem		t_mmem;
typedef struct s_mpage		t_mpage;


struct s_mpage {
	size_t					size;
	struct s_mpage			*next;
};

struct s_mheader {
	size_t					size;
	char					free;
	struct s_mheader		*prev;
};

struct s_mmem {
	void					*tiny;
	void					*small;
	void					*large;
	pthread_mutex_t			access;
	pthread_mutexattr_t		attr;
};

void						*malloc(size_t size);
void						free(void *ptr);
void						*realloc(void *ptr, size_t size);
void						*ft_calloc(size_t n, size_t size);

t_mmem						*ft_get_mem(void);


void						ft_mem_add_pages(t_mpage *cur, size_t size);
void						*ft_malloc_in_pages(t_mpage **pages, size_t size, size_t ref);
void						ft_malloc_add_header(t_mheader *header, size_t size);
void						*ft_mem_create_page(size_t size);

int							ft_ptr_in_page(t_mpage *page, void *ptr);
int							ft_is_end_page(t_mpage *page, t_mheader *header);
int							ft_is_in_page(t_mpage *page, t_mheader *frag);

void						show_alloc_mem(void);

int							ft_strlen(char *str);
void						ft_putstr(char *str);
void						ft_putchar(char c);
void						ft_putnbr(size_t nbr);
void						ft_putaddr(void *ptr);
void						ft_memcpy(void *dest, void const *src, size_t n);

#endif
