/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:09 by triviere          #+#    #+#             */
/*   Updated: 2016/01/29 01:04:29 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void		show_alloc_mem_page(t_mpage *page, size_t *allocated, size_t *freed)
{
	t_mheader		*cur;

	cur = (t_mheader*)((char*)page + sizeof(t_mpage));
	while (!ft_is_end_page(page, cur))
	{
		ft_putchar('	');
		ft_putaddr((void*)cur + sizeof(t_mheader));
		ft_putstr(" - ");
		ft_putaddr((void*)cur + sizeof(t_mheader) + cur->size);
		ft_putstr(" : ");
		ft_putnbr(cur->size);
		ft_putstr(" octets");
		if (cur->free)
			*freed += cur->size;
		else
			*allocated += cur->size;
		ft_putstr(cur->free ? " | FREE\n" : " | ALLOCATED\n");
		cur = (t_mheader*)((char*)cur + cur->size + sizeof(t_mheader));
	}
}

static void		show_alloc_all_page(char *name, t_mpage *page, size_t *allocated, size_t *freed)
{
	t_mpage			*cur;

	ft_putstr(name);
	ft_putstr(" : ");
	ft_putaddr((void *)page + sizeof(t_mpage));
	ft_putstr("\n");
	cur = page;
	while (cur)
	{
		show_alloc_mem_page(cur, allocated, freed);
		ft_putchar('\n');
		cur = cur->next;
	}
}

void				show_alloc_mem(void)
{
	t_mmem		*mem;
	size_t		allocated;
	size_t		freed;

	allocated = 0;
	freed = 0;
	mem = ft_get_mem();
	if (mem->tiny)
		show_alloc_all_page("TINY", mem->tiny, &allocated, &freed);
	if (mem->small)
		show_alloc_all_page("SMALL", mem->small, &allocated, &freed);
	if (mem->large)
		show_alloc_all_page("LARGE", mem->large, &allocated, &freed);
	ft_putstr("Total allocated : ");
	ft_putnbr(allocated);
	ft_putstr(" / ");
	ft_putnbr(allocated + freed);
	ft_putchar('\n');
	ft_putstr("----------------------------------------------------------\n\n");
}