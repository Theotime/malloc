/* ************************************************************************* */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:14 by triviere          #+#    #+#             */
/*   Updated: 2016/01/28 19:20:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"


static void		ft_free_defrag(t_mpage *page, t_mheader *frag)
{
	t_mheader		*cur;
	t_mheader		*next;

	cur = frag;
	while (cur)
	{
		if (!cur->free || !cur->prev || !cur->prev->free)
			break ;
		next = (t_mheader*)((char*)cur + cur->size + sizeof(t_mheader));
		cur->prev->size += cur->size + sizeof(t_mheader);
		next->prev = cur->prev;
		if (cur->prev)
			frag = cur->prev;
		cur = cur->prev;
	}
	(void)page;
	while (!ft_is_end_page(page, cur))
	{
		next = (t_mheader*)((char*)cur + sizeof(t_mheader) + cur->size);
		if (!cur->free || !next->free)
			break ;
		frag->size += next->size + sizeof(t_mheader);
		if (!ft_is_end_page(page, next))
			((t_mheader*)((char*)next + next->size + sizeof(t_mheader)))->prev = frag;
		next->prev = cur->prev;
		cur = next;
	}
}

int				ft_is_in_page(t_mpage *page, t_mheader *frag)
{
	t_mheader	*cur;

	cur = (t_mheader*)((char*)page + sizeof(t_mpage));
	while (!ft_is_end_page(page, cur))
	{
		if ((char*)cur == (char*)frag)
			return (1);
		if ((char*)cur > (char*)frag)
			return (0);
		cur = (t_mheader*)((char*)cur + sizeof(t_mheader) + cur->size);
	}
	return (0);
}

static void		ft_check_free_page(t_mpage *prev, t_mpage *cur)
{
	t_mheader	*head;

	head = (t_mheader*)((char*)cur + sizeof(t_mpage));
	head = (t_mheader*)((char*)head + head->size + sizeof(t_mheader));
	if (!ft_is_end_page(cur, head))
		return ;
	prev->next = cur->next;
	munmap(cur, cur->size + sizeof(t_mpage));
}

static int		ft_free_in_pages(t_mpage *page, void *ptr)
{
	t_mpage		*cur;
	t_mpage		*prev;
	t_mheader	*tmp;

	cur = page;
	prev = NULL;
	tmp = (t_mheader*)((char*)ptr - sizeof(t_mheader));
	while (cur)
	{
		if (ft_ptr_in_page(cur, ptr))
		{
			if (!ft_is_in_page(cur, tmp))
				return (0);
			tmp->free = 1;
			ft_free_defrag(cur, tmp);
			if (prev != NULL)
				ft_check_free_page(prev, cur);
			return (1);
		}
		prev = cur;
		cur = cur->next;
	}
	return (0);
}

void			free(void *ptr)
{
	t_mmem		*mem;

	if (ptr == NULL)
		return ;
	mem = ft_get_mem();
	pthread_mutex_lock(&(mem->access));
	ft_free_in_pages(mem->tiny, ptr)
	|| ft_free_in_pages(mem->small, ptr)
	|| ft_free_in_pages(mem->large, ptr);
	pthread_mutex_unlock(&(mem->access));
}
