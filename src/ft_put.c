#include "malloc.h"

int				ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		++i;
	return (i);
}

void			ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void			ft_putchar(char c)
{
	write(1, &c, 1);
}

static	void	ft_putnbr_rec(size_t nbr)
{
	char	c;

	if (!nbr)
		return ;
	c = nbr % 10 + 48;
	ft_putnbr_rec(nbr / 10);
	ft_putchar(c);
}

void			ft_putnbr(size_t nbr)
{
	if ((int)nbr < 0)
	{
		ft_putchar('-');
		ft_putnbr_rec(nbr * -1);
	}
	else if (nbr)
		ft_putnbr_rec(nbr);
	else
		ft_putchar('0');
}