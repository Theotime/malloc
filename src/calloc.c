#include "malloc.h"

void		*ft_calloc(size_t n, size_t size)
{
	void		*dest;
	size_t		len;
	size_t		i;

	len = n * size;
	if (size == 0)
		return (NULL);
	dest = malloc(len);
	if (!dest)
		return (NULL);
	i = 0;
	while (i < len)
		((char*)dest)[i++] = 0;
	return (dest);
}