/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:33 by triviere          #+#    #+#             */
/*   Updated: 2016/01/29 13:30:09 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void		*ft_mem_create_memory(size_t size)
{
	void	*mem;

	mem = mmap(0, size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
	return (mem);
}

static void		ft_malloc_init_header(t_mpage *page)
{
	t_mheader	*header;

	header = (t_mheader*)((char*)page + sizeof(t_mpage));
	header->size = page->size - sizeof(t_mheader);
	header->free = 1;
	header->prev = NULL;
}

int				ft_ptr_in_page(t_mpage *page, void *ptr)
{
	char		more;
	char		less;

	more = (char*)ptr > (char*)page;
	less = (char*)ptr < (char*)page + page->size + sizeof(t_mpage) + sizeof(t_mheader);
	// ft_putaddr(ptr);
	// ft_putstr(" > ");
	// ft_putaddr(page - sizeof(t_mpage));
	// ft_putstr(" == ");
	// ft_putnbr(more);
	// ft_putstr(" && ");
	// ft_putaddr(ptr);
	// ft_putstr(" < ");
	// ft_putaddr((char*)page + page->size + sizeof(t_mpage));
	// ft_putstr(" == ");
	// ft_putnbr(less);
	// ft_putstr(" | RETURN : ");
	// ft_putnbr(more && less);
	// ft_putchar('\n');
	return (more && less);
}

void			*ft_mem_create_page(size_t size)
{
	static int	ps = 0;
	void		*mem;
	t_mpage		*page;
	size_t		len;

	if (!ps)
		ps = getpagesize();
	len = sizeof(t_mpage) + size * (size <= SMALL ? NB_PAGE : 1);
	if (size <= SMALL)
		len += (sizeof(t_mheader) * NB_PAGE);
	else
		len += sizeof(t_mheader);
	len += len % ps;
	mem = ft_mem_create_memory(len);
	if (mem == NULL)
		return (NULL);
	page = (t_mpage*)mem;
	page->size = len - sizeof(t_mpage);
	page->next = NULL;
	ft_malloc_init_header(page);
	return (mem);
}

void			ft_mem_add_pages(t_mpage *cur, size_t size)
{
	cur->next = ft_mem_create_page(size);
}

t_mmem			*ft_get_mem(void)
{
	static t_mmem		mem;
	static int			init = 0;

	if (!init)
	{
		pthread_mutexattr_init(&mem.attr);
		pthread_mutexattr_settype(&mem.attr, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&mem.access, &mem.attr);
		mem.tiny = NULL;
		mem.small = NULL;
		mem.large = NULL;
		init = 1;
	}
	return (&mem);
}
