#include "malloc.h"

void			ft_putaddr(void *ptr)
{
	size_t			adr;
	char			*tab;
	char			ret[9];
	int				i;

	adr = (size_t)ptr;
	tab = "0123456789abcdef";
	i = 8;
	while ((adr / 16) > 0 || i >= 8)
	{
		ret[i] = tab[(adr % 16)];
		adr /= 16;
		i--;
	}
	ret[i] = tab[(adr % 16)];
	write(1, "0x", 2);
	while (i < 9)
		write(1, &ret[i++], 1);
}