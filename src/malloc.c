/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:29 by triviere          #+#    #+#             */
/*   Updated: 2016/01/29 13:34:13 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void		*ft_malloc_in_page(t_mpage *page, size_t size)
{
	t_mheader	*cur;

	cur = (t_mheader*)((char*)page + sizeof(t_mpage));
	while (cur)
	{
		if (cur->free && cur->size >= size)
		{
			cur->free = 0;
			ft_malloc_add_header(cur, size);
			return ((char*)cur + sizeof(t_mheader));
		}
		cur = (t_mheader*)((char *)cur + cur->size + sizeof(t_mheader));
		if (ft_is_end_page(page, cur))
			break ;
	}
	return (NULL);
}

void			*ft_malloc_in_pages(t_mpage **pages, size_t size, size_t ref)
{
	void		*tmp;
	t_mpage		*cur;

	if (*pages == NULL)
		*pages = ft_mem_create_page(ref);
	cur = *pages;
	while (cur)
	{
		tmp = ft_malloc_in_page(cur, size);
		if (tmp != NULL)
			return (tmp);
		if (cur->next == NULL)
		{
			ft_mem_add_pages(cur, ref);
			return (ft_malloc_in_page(cur->next, size));
		}
		cur = cur->next;
	}
	return (NULL);
}

void			*malloc(size_t size)
{
	void			*tmp;
	t_mmem			*mem;

	mem = ft_get_mem();
	pthread_mutex_lock(&(mem->access));
	if (size <= TINY) 
		tmp = (ft_malloc_in_pages((t_mpage**)&(mem->tiny), size, TINY));
	else if (size <= SMALL)
		tmp = (ft_malloc_in_pages((t_mpage**)&(mem->small), size, SMALL));
	else
		tmp = (ft_malloc_in_pages((t_mpage**)&(mem->large), size, size));
	pthread_mutex_unlock(&(mem->access));
	return (tmp);
}
