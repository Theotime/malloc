/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:41 by triviere          #+#    #+#             */
/*   Updated: 2016/01/29 13:34:32 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static t_mheader		*ft_get_frag(t_mpage *page, void *ptr, t_mpage **p)
{
	t_mpage		*cur;
	t_mheader	*tmp;

	cur = page;
	tmp = ((t_mheader*)((char *)ptr - sizeof(t_mheader)));
	while (cur)
	{
		if (ft_ptr_in_page(cur, tmp))
		{
			if (!ft_is_in_page(cur, tmp))
				return (NULL);
			*p = cur;
			return tmp;
		}
		cur = cur->next;
	}
	return (NULL);
}

static void				*ft_realloc(t_mpage *page, t_mheader *src, size_t size)
{
	void		*dest;
	size_t		tmp;
	t_mheader	*nxt;
	t_mheader	*end;

	if (!ft_is_end_page(page, src))
	{
		nxt = (t_mheader*)((char*)src + src->size + sizeof(t_mheader));
		if (!ft_is_end_page(page, nxt) && nxt->free && nxt->size + sizeof(t_mheader) >= size)
		{
			end = (t_mheader*)((char*)nxt + sizeof(t_mheader) + nxt->size);
			tmp = src->size;
			src->size += nxt->size + sizeof(t_mheader);
			ft_malloc_add_header(src, size);
			if (!ft_is_end_page(page, end) && (char*)src + src->size + sizeof(t_mheader) != (char*)end)
				end->prev = (t_mheader*)((char*)src + src->size + sizeof(t_mheader));
			else
				end->prev = src;
			return ((t_mheader*)((char*)src + sizeof(t_mheader)));
		}

	}
	dest = malloc(size);
	ft_memcpy(dest, (void*)((char*)src + sizeof(t_mheader)), src->size);
	free((void*)((char*)src + sizeof(t_mheader)));
	return (dest);
}

void					*realloc(void *ptr, size_t size)
{
	t_mmem		*mem;
	t_mheader	*src;
	t_mpage		*page;
	void		*dest;

	if (ptr == NULL)
		return (malloc(size));
	if (size == 0)
	{
		free(ptr);
		return (malloc(sizeof(char)));
	}
	mem = ft_get_mem();
	pthread_mutex_lock(&(mem->access));
	src = ft_get_frag(mem->tiny, ptr, &page);
	if (src == NULL)
		src = ft_get_frag(mem->small, ptr, &page);
	if (src == NULL)
		src = ft_get_frag(mem->large, ptr, &page);
	if (!src)
		dest = NULL;
	else
		dest = ft_realloc(page, src, size);
	pthread_mutex_lock(&(mem->access));
	return (dest);
}
