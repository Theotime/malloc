#include "malloc.h"

/*
int			main()
{
	char		*str1;
	char		*str2;
	char		*str3;
	char		*str4;
	char		*str5;

	show_alloc_mem();
	str1 = malloc(24);
	show_alloc_mem();
	str2 = malloc(42);
	show_alloc_mem();
	str3 = malloc(203);
	show_alloc_mem();
	str4 = malloc(3534);
	show_alloc_mem();
	str5 = malloc(10024);
	show_alloc_mem();


	free(str2);
	str1 = realloc(str1, 240);
	show_alloc_mem();

	return (0);
}
*/

int			main()
{
	char		*str1;
	char		*str2;
	char		*str3;
	int			i;

	i = 0;
	while (i < 10)
	{
		str1 = (char*)malloc(10);
		str2 = (char*)malloc(50);
		str3 = (char*)malloc(10);

		str1[0] = 42;
		str2[0] = 42;
		str3[0] = 42;

		free(str2);
		show_alloc_mem();
		str1 = (char*)realloc(str1, 40);
		free(str1);
		free(str3);

		++i;
	}
	show_alloc_mem();
	return (0);
}
