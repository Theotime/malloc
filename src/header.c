/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 15:30:21 by triviere          #+#    #+#             */
/*   Updated: 2016/01/29 01:56:00 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		ft_malloc_add_header(t_mheader *header, size_t size)
{
	t_mheader		*new;

	if (header->size <= size + sizeof(t_mheader) || size > SMALL)
		return ;
	new = (t_mheader*)((char*)header + sizeof(t_mheader) + size);
	new->size = header->size - sizeof(t_mheader) - size;
	new->free = 1;
	new->prev = header;
	header->size = size;
}

int		ft_is_end_page(t_mpage *page, t_mheader *header)
{
	return ((char*)header >= (char*)page + page->size + sizeof(t_mpage));
}
