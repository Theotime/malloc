ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

CC=gcc
DIR_SRC = src

SRCS=$(shell find $(DIR_SRC) -type f)

INCLUDES=-Iinclude

CFLAGS=-Wall -Werror -Wextra $(INCLUDES)
LDFLAGS=

NAME = test
NAME = libft_malloc_$(HOSTTYPE).so
LINK = libft_malloc.so

OBJS=$(patsubst src/%,obj/%, $(SRCS:.c=.o))

all: $(NAME)
	

re : fclean all

clean:
	rm -rf obj

fclean: clean
	rm -rf ${NAME}

$(NAME): $(OBJS)
	# $(CC) -o $@ $^ $(LDFLAGS)
	$(CC) -shared -o $@ $^ $(LDFLAGS)
	ln -Ffs $(NAME) $(LINK)

obj/%.o : src/%.c
	mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(CFLAGS)

run:
	./$(NAME) X 42

.PHONY: clean fclean re all $(NAME)
