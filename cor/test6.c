#include "malloc.h"

int main() {
	char	*ptr[1000];
	int		i = 0;
	int		n = 1000;

	while (i < n)
	{
		ptr[i] = (char*)malloc(1024);
		if (ptr[i] == NULL)
			return (0);
		ptr[i][0] = 42;
		++i;
	}
	show_alloc_mem();
	i = 0;
	while (i < n)
	{
		ptr[i] = (char*)realloc(ptr[i], 2048);
		ptr[i][0] = 42;
		++i;
	}
	show_alloc_mem();
	i = 0;
	while (i < n)
	{
		free(ptr[i]);
		++i;
	}
	show_alloc_mem();
	
    return 0;
}
